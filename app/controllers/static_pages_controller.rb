class StaticPagesController < ApplicationController
  def home
  end

  def landing
  end

  def thanks
  end

  def phase1
  end

  def phase2
  end

  def phase3
  end
end
