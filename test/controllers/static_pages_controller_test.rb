require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get phase1" do
    get :phase1
    assert_response :success
  end

  test "should get phase2" do
    get :phase2
    assert_response :success
  end

  test "should get phase3" do
    get :phase3
    assert_response :success
  end

end
